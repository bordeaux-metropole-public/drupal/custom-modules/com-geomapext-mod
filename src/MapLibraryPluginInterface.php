<?php

namespace Drupal\geofield_map_ext;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\geofield_map_ext\Plugin\Field\FieldWidget\GeofieldMapExtWidget;

/**
 * Provides an interface for Geofield Map Ext Libraries plugins.
 */
interface MapLibraryPluginInterface extends PluginInspectionInterface {

  /**
   *
   */
  public function mapWidgetDefaultSettings(array &$default_settings);

  /**
   *
   */
  public function mapWidgetSettingsForm(GeofieldMapExtWidget $widget, array &$elements);

  /**
   *
   */
  public function mapWidgetSettingsSummary(GeofieldMapExtWidget $widget, array &$summary);

  /**
   *
   */
  public function mapWidgetFormElement(GeofieldMapExtWidget $widget, array &$element);

  /**
   *
   */
  public function mapFormElement(array &$element);

  /**
   *
   */
  public function mapWidgetWebformSettings(&$elements);

  /**
   *
   */
  public function mapWidgetWebformElement(&$element);

}
