<?php

namespace Drupal\geofield_map_ext;

interface GeofieldMapFieldInterface {

  public function getFieldName();

  public function getLink();

}
