<?php

namespace Drupal\geofield_map_ext\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a MapLibraryPlugin item annotation object.
 *
 * @see \Drupal\geofield_map_ext\MapLibraryPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
class MapLibraryPlugin extends Plugin {

  /**
   * The  Map Library plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
